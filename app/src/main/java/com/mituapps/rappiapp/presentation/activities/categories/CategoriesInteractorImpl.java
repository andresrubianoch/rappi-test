package com.mituapps.rappiapp.presentation.activities.categories;

/**
 * Created by Andres Rubiano Del Chiaro on 17/09/2016.
 */
public class CategoriesInteractorImpl implements CategoriesInteractor {

    private CategoriesRespository categoriesRespository;

    public CategoriesInteractorImpl() {
        categoriesRespository = new CategoriesRepositoryImpl();
    }

    @Override
    public void downloadCategories() {
        categoriesRespository.downloadCategories();
    }
}

package com.mituapps.rappiapp.presentation.activities;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import com.mituapps.rappiapp.R;
import com.mituapps.rappiapp.presentation.navigation.Navigator;

import org.androidannotations.annotations.EActivity;

/**
 * Created by Andres Rubiano Del Chiaro on 13/09/2016.
 */

@EActivity
public abstract class BaseActivity extends AppCompatActivity{
    Navigator mNavigator;

    protected boolean bIsLargeScreen = false;
    private final static boolean IS_LARGE_SCREEN = true;
    private final static boolean IS_NOT_LARGE_SCREEN = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setOrientationBusiness();
        super.onCreate(savedInstanceState);
        initNavigator();
    }

    private void initNavigator() {
        mNavigator = new Navigator();
    }

    private void setOrientationBusiness() {
        int typeOrientation = -1;
        if(getResources().getBoolean(R.bool.portrait_only)){
            typeOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
            setLargeScreen(IS_NOT_LARGE_SCREEN);
        } else {
            typeOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
            setLargeScreen(IS_LARGE_SCREEN);
        }

        if (typeOrientation >= 0){
            changeOrientation(typeOrientation);
        }
    }

    private void changeOrientation(@NonNull int type){
        setRequestedOrientation(type);
    }

    private void setLargeScreen(boolean flag){
        bIsLargeScreen = flag;
    }

    protected void hideView(View v){v.setVisibility(View.GONE);}

    protected void showView(View v){v.setVisibility(View.VISIBLE);}

    protected void addFragment(@NonNull int layout, @NonNull Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(layout, fragment);
        transaction.commit();
    }

    protected void showSnackbarMessage(@NonNull ViewGroup activity, @NonNull String message){
        Snackbar
                .make(activity, message, Snackbar.LENGTH_SHORT)
                .show();
    }

//    @AfterInject
//    protected void init(){
//        getApplicationComponent().inject(this);
//    }

//    public ApplicationComponent getApplicationComponent() {
//        return ((AndroidApplicatoin) getApplication().getApplicationComponent());
//    }

}

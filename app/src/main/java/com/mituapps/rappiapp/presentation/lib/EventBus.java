package com.mituapps.rappiapp.presentation.lib;

/**
 * Created by Andres Rubiano Del Chiaro on 17/09/2016.
 */
public interface EventBus {

    void register(Object subscriber);
    void unregister(Object subscriber);
    void post(Object event);
}

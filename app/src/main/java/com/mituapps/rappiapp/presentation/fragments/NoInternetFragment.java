package com.mituapps.rappiapp.presentation.fragments;

import android.support.v4.app.Fragment;

import com.mituapps.rappiapp.R;

import org.androidannotations.annotations.EFragment;

@EFragment(R.layout.help_no_internet)
public class NoInternetFragment extends Fragment {

}

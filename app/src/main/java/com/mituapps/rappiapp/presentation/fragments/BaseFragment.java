package com.mituapps.rappiapp.presentation.fragments;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.mituapps.rappiapp.presentation.activities.BaseActivity;
import com.mituapps.rappiapp.presentation.navigation.Navigator;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

import javax.inject.Inject;

/**
 * Created by Andres Rubiano Del Chiaro on 13/09/2016.
 */
@EFragment
public abstract class BaseFragment extends Fragment {
    @Inject
    Navigator mNavigator;

//    @AfterInject
//    protected void init() {
//        getApplicationComponent().inject(this);
//    }
//
//    @AfterViews
//    protected void initViews() {}
//
//    protected ApplicationComponent getApplicationComponent() {
//        return ((BaseActivity)getActivity()).getApplicationComponent();
//    }

    protected void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }
}

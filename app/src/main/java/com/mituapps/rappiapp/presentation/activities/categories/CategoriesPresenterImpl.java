package com.mituapps.rappiapp.presentation.activities.categories;

/**
 * Created by Andres Rubiano Del Chiaro on 17/09/2016.
 */
public class CategoriesPresenterImpl implements CategoriesPresenter {

    CategoriesView categoriesView;
    CategoriesInteractor categoriesInteractor;

    public CategoriesPresenterImpl(CategoriesView categoriesView) {
        this.categoriesView = categoriesView;
        categoriesInteractor = new CategoriesInteractorImpl();
    }

    @Override
    public void onDestroy() {
        categoriesView = null;
    }

    @Override
    public void downloadCategories() {
        categoriesInteractor.downloadCategories();
    }
}

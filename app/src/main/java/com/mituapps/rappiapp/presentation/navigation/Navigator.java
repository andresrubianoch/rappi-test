package com.mituapps.rappiapp.presentation.navigation;

import android.content.Context;

import com.mituapps.rappiapp.presentation.activities.AppsDetailActivity_;
import com.mituapps.rappiapp.presentation.activities.AppsListActivity_;
import com.mituapps.rappiapp.presentation.activities.categories.CategoriesActivity_;

/**
 * Created by Andres Rubiano Del Chiaro on 13/09/2016.
 */
public class Navigator {

    public Navigator(){}

    public void toCategoriesActivity(Context context){
        if (null != context)
            CategoriesActivity_.intent(context).start();
    }

    public void toAppListActivity(Context context){
        if (null != context)
            AppsListActivity_.intent(context).start();
    }

    public void toAppDetailActivity(Context context){
        if (null != context)
            AppsDetailActivity_.intent(context).start();
    }
}

package com.mituapps.rappiapp.presentation.activities.categories;

/**
 * Created by Andres Rubiano Del Chiaro on 16/09/2016.
 */
public interface CategoriesPresenter {
    void onDestroy();
    void downloadCategories();
}

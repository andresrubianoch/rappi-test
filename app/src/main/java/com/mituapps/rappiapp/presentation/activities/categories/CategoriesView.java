package com.mituapps.rappiapp.presentation.activities.categories;

/**
 * Created by Andres Rubiano Del Chiaro on 16/09/2016.
 */
public interface CategoriesView {

    void enableInputs();
    void disableInputs();

    void showProgress();
    void hideProgress();

    void noInternetConnection();
    void downloadError();
    void successDownload();

    void loadInfo();
}

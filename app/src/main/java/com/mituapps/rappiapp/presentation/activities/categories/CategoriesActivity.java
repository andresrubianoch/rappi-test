package com.mituapps.rappiapp.presentation.activities.categories;

import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.mituapps.rappiapp.R;
import com.mituapps.rappiapp.presentation.activities.BaseActivity;
import com.mituapps.rappiapp.presentation.fragments.NoInternetFragment_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_categories)
public class CategoriesActivity extends BaseActivity implements CategoriesView {

    CategoriesPresenter categoriesPresenter;

    @ViewById
    ProgressBar progressBarCategories;

    @ViewById
    FrameLayout containerInfoCategories;

    @ViewById
    RelativeLayout parentRelative;

    @Override
    public void enableInputs() {
        showView(containerInfoCategories);
    }

    @Override
    public void disableInputs() {
        hideView(containerInfoCategories);
    }

    @Override
    public void showProgress() {
        showView(progressBarCategories);
    }

    @Override
    public void hideProgress() {
        hideView(progressBarCategories);
    }

    @Override
    public void noInternetConnection() {
        addFragment(R.id.containerInfoCategories, new NoInternetFragment_());
    }

    @Override
    public void downloadError() {
        showSnackbarMessage(parentRelative, getString(R.string.categories_error));
    }

    @Override
    public void successDownload() {

    }

    @AfterViews
    @Override
    public void loadInfo() {
        categoriesPresenter = new CategoriesPresenterImpl(CategoriesActivity.this);
        categoriesPresenter.downloadCategories();
        noInternetConnection();
        hideProgress();
        enableInputs();
    }
}
